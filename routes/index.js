var express = require('express');
var app = express();
var router = express.Router();

const messages = [
  {text: "Hi there!", user: "Amando", added: new Date()}, 
  {text: "Hello World!", user: "Charles", added: new Date() } ];


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {title: "Mini Message Board", messages: messages});
});

router.post('/newmessage', function(req, res) {
  messages.push({text: req.body.message, user: req.body.username, added: new Date()});
  res.redirect('/');
})

module.exports = router;
